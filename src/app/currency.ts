export class Currency {

  currencySymbol: string;
  currencyName: string;
  relativeValue: {};      //1 of this currency =  x of all other currencies

  constructor(currencySymbol: string, currencyName: string) {
    this.currencySymbol = currencySymbol;
    this.currencyName = currencyName;
    this.relativeValue = {};
  }

}
