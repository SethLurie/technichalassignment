import { Injectable } from '@angular/core';
import {Currency} from "./currency";
import {CurrencyNode} from "./currency-node";

@Injectable({
  providedIn: 'root'
})
export class CurrencyService {

  private host = 'frankfurter.app';
  public selectedCurrency: Currency;
  public currencies: {} = {};
  public currencySymbols: Array<string> = [];
  public bestTrade: any[];
  public amountInvested = 1;
  private currencyNodes: {} = {};
  private selectedCurrencySymbol: string;

  constructor() {
  }

  getCurrencies() :void {
    //gets exchange rates for each currency
    fetch(`https://${this.host}/currencies`)
      .then(resp => resp.json())
      .then((data) => {
        this.currencySymbols = Object.keys(data);

        this.currencySymbols.forEach(i => {
          let newCurrency = new Currency(i, data[i]);
          this.getCurrencyRate(newCurrency.currencySymbol).then(
            (data) => {
              newCurrency.relativeValue = data.rates;
            }
          );
          this.currencies[newCurrency.currencySymbol] = newCurrency;
        });

      })
    //use to select a default currency
      // .then(() => {
      //   this.selectedCurrency = this.currencies[this.currencySymbols[0]];
      // });
  }

  private getCurrencyRate(currencySymbol) : Promise<any | never> {
    //returns a single currency exchange rate
    return fetch(`https://${this.host}/latest?from=${currencySymbol}`)
      .then(resp => resp.json())
      .then((data) => {
        return data;
      });
  }

  currencySelected() : void {
    //runs every single time a new currency is selected
    this.currencyNodes = {};
    this.selectedCurrencySymbol = this.selectedCurrency.currencySymbol;
    for (let i of this.currencySymbols) {
      this.currencyNodes[i] = new CurrencyNode(this.currencies[i]);
    }
    this.currencyNodes[this.selectedCurrency.currencySymbol].distance = 0;
    this.getAllMinDistancesForward();
    this.getMinDistanceBack();
    this.getTrades();
    this.getTradeAmounts();
    console.log(this.bestTrade);
  }

  private getAllMinDistancesForward() : void {
    // gets all of the minimum forward distance from selected node to each other node
    // no loops are allowed
    for (let i = 0; i < this.currencySymbols.length - 1; i++) {
      for (let j of this.currencySymbols) {
        if (this.currencyNodes[j].distance != Infinity) {
          for (let w of this.currencySymbols) {
            const startingDist = this.currencyNodes[j].distance;
            const edgeDist = this.currencyNodes[j].edgeValues[w];
            const distToW = startingDist + edgeDist;
            if (distToW < this.currencyNodes[w].distance
              && !CurrencyService.cont(this.currencyNodes[w], this.currencyNodes[j])) {
                this.currencyNodes[w].distance = distToW;
                this.currencyNodes[w].prevCurrency = this.currencyNodes[j];
            }
          }
        }
      }
    }
  }

  private static cont(cn1: any, cn2: any) : boolean {
    // function to determine if a loop is occurring
    if(!cn2.prevCurrency ){
      return  false;
    }
    //returns false when prevCurrency is null i.e. when node = selectedCurrency
    let found = false;
    while(cn2.prevCurrency && !found) {
      //while the starting node has not been found
      if (cn2.prevCurrency == cn1){
        found = true;
      }
      cn2 = cn2.prevCurrency;
    }
    return found;
  }

  amountChanged() : void {
    //triggered when the amount invested changes
    this.getTradeAmounts();
  }

  private getMinDistanceBack() : void {
    //given all forward distances, find min distance back to selectedCurrency
    for (let i of this.currencySymbols) {
      let distanceBack = this.currencyNodes[i].edgeValues[this.selectedCurrencySymbol];
      if (distanceBack < this.currencyNodes[this.selectedCurrencySymbol].distance) {
        this.currencyNodes[this.selectedCurrencySymbol].distance = distanceBack;
        this.currencyNodes[this.selectedCurrencySymbol].prevCurrency = this.currencyNodes[i];
      }
    }
  }

  private getTrades() : void {
    //gets best trade array
    this.bestTrade = [this.currencyNodes[this.selectedCurrencySymbol]];
    while (
      this.bestTrade.indexOf(this.bestTrade[this.bestTrade.length - 1]) ===
      this.bestTrade.length - 1
      //while the last added index only occurs once
      //this condition is used so that thi method works when internal cycles are allowed
      ) {
      this.bestTrade.push(this.bestTrade[this.bestTrade.length - 1].prevCurrency);
    }
    this.bestTrade = this.bestTrade.reverse();
  }

  private getTradeAmounts() : void {
    //gets amounts used in best trade
    this.bestTrade[0].amount = this.amountInvested;
    for (let i = 1; i < this.bestTrade.length; i++) {
      this.bestTrade[i].amount =
        this.bestTrade[i-1].amount *
        this.bestTrade[i-1].currency.relativeValue[this.bestTrade[i].currency.currencySymbol];
    }

  }
}
