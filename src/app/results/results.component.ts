import { Component, OnInit } from '@angular/core';
import {CurrencyService} from "../currency.service";

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.css']
})
export class ResultsComponent implements OnInit {

  constructor(public currency: CurrencyService) { }

  ngOnInit() {

  }

}
