import {Currency} from "./currency";

export class CurrencyNode {
  currency : Currency;
  prevCurrency : CurrencyNode = null ;
  distance : number = Infinity;
  edgeValues : {} = {};
  amount : number;

  constructor(currency: Currency) {
    this.currency = currency;
    let currencySymbols = Object.keys(this.currency.relativeValue);
    this.amount = 0;
    for (let i of currencySymbols) {
      this.edgeValues[i] = -Math.log(this.currency.relativeValue[i]);
    }
  }
}
