import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CurrencyPickerComponent } from './currency-picker/currency-picker.component';
import { FormsModule } from "@angular/forms";
import { NodeComponent } from './node/node.component';
import { ResultsComponent } from './results/results.component';

import { CurrencyService } from './currency.service';

@NgModule({
  declarations: [
    AppComponent,
    CurrencyPickerComponent,
    NodeComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [CurrencyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
