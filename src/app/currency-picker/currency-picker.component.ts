import { Component, OnInit } from '@angular/core';
import {CurrencyService} from "../currency.service";
import {getCurrencySymbol} from "@angular/common";

@Component({
  selector: 'app-currency-picker',
  templateUrl: './currency-picker.component.html',
  styleUrls: ['./currency-picker.component.css']
})
export class CurrencyPickerComponent implements OnInit {

  constructor(public currency : CurrencyService) { }

  ngOnInit() {
    this.currency.getCurrencies();
  }

  getSymbol(currencySymbol: string) : string {
    return getCurrencySymbol(currencySymbol,"narrow");
  }
}
